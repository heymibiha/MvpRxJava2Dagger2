package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader;

import io.reactivex.Flowable;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.base.IBasePresenter;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.base.IBaseView;

public class LoaderContract {
    public interface View extends IBaseView {
        void setCurrentIp(String text);

        void showError(String errorText);

        void showLoading();

        void hideLoading();
    }

    public interface Presenter extends IBasePresenter<View> {
        void loadData();
    }
}

package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.model;

import com.google.gson.annotations.SerializedName;

public class IpAddress {

    @SerializedName("origin")
    private String currentIp;

    public String getCurrentIp() {
        return currentIp;
    }
}

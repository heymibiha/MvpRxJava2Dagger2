package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.base;

import dagger.android.DaggerFragment;

public abstract class BasePresenter<T extends IBaseView> extends DaggerFragment implements IBasePresenter<T> {

    T mView;

    @Override
    public void removeView() {
        mView = null;
    }

    @Override
    public void bindView(T view) {
        mView = view;
    }

    protected T getFragmentView() {
        return mView;
    }
}

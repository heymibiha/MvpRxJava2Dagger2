package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di;

import android.app.Application;

import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.components.DaggerMainApplicationComponent;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.components.MainApplicationComponent;


public class MainGlobalState {
    private static MainGlobalState sInstance;
    private final MainApplicationComponent mComponent;

    private MainGlobalState(Application application) {
        mComponent =
                DaggerMainApplicationComponent.builder()
                        .application(application)
                        .build();
    }

    public static MainApplicationComponent get(Application application) {
        if (sInstance == null) {
            sInstance = new MainGlobalState(application);
        }

        return sInstance.get();
    }

    public MainApplicationComponent get() {
        return mComponent;
    }
}

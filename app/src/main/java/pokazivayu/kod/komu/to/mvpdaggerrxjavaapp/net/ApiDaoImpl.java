package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net;

import java.lang.reflect.Method;

import javax.inject.Inject;

import io.reactivex.Flowable;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.model.IpAddress;

public class ApiDaoImpl implements IApiDao {
    final IApiClient mApiClient;

    @Inject
    public ApiDaoImpl(IApiClient mApiClient) {

        this.mApiClient = mApiClient;
    }

    @Override
    public Flowable<IpAddress> getCurrentIp() {
        Method method = null;

        try {
            method = getClass().getInterfaces()[0].getMethod("getCurrentIp");
        } catch (NoSuchMethodException e) {
            Flowable.error(e);
        }

        RequestMethod annotation = method.getAnnotation(RequestMethod.class);
        String path = annotation.value();
        return mApiClient.get(path, IpAddress.class);
    }
}

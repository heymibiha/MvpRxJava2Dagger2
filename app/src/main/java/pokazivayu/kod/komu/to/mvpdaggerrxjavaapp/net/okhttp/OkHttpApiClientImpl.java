package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.okhttp;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.IApiClient;

public class OkHttpApiClientImpl implements IApiClient {

    final OkHttpClient mOkHttpClient;

    final String mBaseUrl;

    final Gson mGson;

    @Inject
    public OkHttpApiClientImpl(String baseUrl, Gson gson) {
        mBaseUrl = baseUrl;
        mGson = gson;
        mOkHttpClient = new OkHttpClient.Builder().build();
    }

    public <T> Flowable<T> get(String method, Class<T> resultClass) {
        Request request = new Request.Builder()
                .url(HttpUrl.parse(mBaseUrl + method))
                .build();

        return Flowable.defer(this.prepareResponseFlowable(request, resultClass));
    }

    private <T> Callable<Flowable<T>> prepareResponseFlowable(Request request, Class<T> resultClass) {
        return () -> {

            Response response = null;

            try {
                response = mOkHttpClient.newCall(request).execute();

            } catch (IOException e) {
                return Flowable.error(e);
            }


            if (response != null && response.isSuccessful()) {
                T entity = OkHttpApiClientImpl.this.parseJson(response.body().string(), resultClass);

                return Flowable.just(entity);
            }

            return Flowable.error(new Throwable("Network showError"));

        };
    }

    private <T> T parseJson(String json, Class<T> resultClass) {
        return mGson.fromJson(json, resultClass);
    }
}

package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class MainApplicationModule {
    @Binds
    abstract Context bindContext(Application application);

}

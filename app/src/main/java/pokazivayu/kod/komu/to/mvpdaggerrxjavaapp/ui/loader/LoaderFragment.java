package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.CompositeDisposable;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.R;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.ActivityScoped;

@ActivityScoped
public class LoaderFragment extends DaggerFragment implements LoaderContract.View {

    @Inject
    LoaderContract.Presenter mPresenter;

    @BindView(R.id.loader_tv_current_ip)
    TextView mCurrentIpTv;

    @BindView(R.id.loader_button_load)
    Button mLoadBtn;

    @BindView(R.id.loader_pb_loading)
    ProgressBar mLoadingPb;

    @BindString(R.string.loader_ip_format)
    String mIpFormat;

    @BindString(R.string.loader_error_format)
    String mErrorFormat;

    final CompositeDisposable mCd;

    @Inject
    public LoaderFragment() {
        mCd = new CompositeDisposable();
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.bindView(this);
    }

    @Override
    public void onPause() {
        mPresenter.removeView();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mCd.clear();
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.loader_fragment_layout, container, false);
        ButterKnife.bind(this, view);

        mCd.add(RxView.clicks(mLoadBtn).toFlowable(BackpressureStrategy.LATEST).subscribe(aVoid -> mPresenter.loadData()));

        return view;
    }

    @Override
    public void setCurrentIp(String text) {
        mCurrentIpTv.setText(String.format(mIpFormat, text));
    }

    @Override
    public void showError(String errorText) {
        mCurrentIpTv.setText(String.format(mErrorFormat, errorText));
    }

    @Override
    public void showLoading() {
        mLoadBtn.setEnabled(false);
        mLoadingPb.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mLoadingPb.setVisibility(View.GONE);
        mLoadBtn.setEnabled(true);
    }
}
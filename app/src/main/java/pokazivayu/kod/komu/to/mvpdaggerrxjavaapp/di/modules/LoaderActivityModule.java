package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.ActivityScoped;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.FragmentScoped;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader.LoaderFragment;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader.LoaderPresenter;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader.LoaderContract;

@Module
public abstract class LoaderActivityModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract LoaderFragment loaderFragment();

    @ActivityScoped
    @Binds
    abstract LoaderContract.Presenter loaderPresenter(LoaderPresenter presenter);
}

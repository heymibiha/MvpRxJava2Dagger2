package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net;

import io.reactivex.Flowable;

//TODO i.e. can be extended with request types
public interface IApiClient {
    <T> Flowable<T> get(String method, Class<T> resultClass);
}

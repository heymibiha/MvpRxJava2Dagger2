package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.ActivityScoped;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader.LoaderActivity;

@Module
public abstract class ActivityBinderModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = {LoaderActivityModule.class})
    abstract LoaderActivity loaderActivity();
}

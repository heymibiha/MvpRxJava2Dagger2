package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.base;

public interface IBasePresenter<T> {
    void removeView();

    void bindView(T view);
}

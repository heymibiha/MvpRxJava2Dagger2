package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net;

import io.reactivex.Flowable;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.model.IpAddress;

public interface IApiDao {
    @RequestMethod("/ip")
    Flowable<IpAddress> getCurrentIp();
}

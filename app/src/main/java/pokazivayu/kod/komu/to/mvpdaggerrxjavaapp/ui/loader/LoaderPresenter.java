package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.loader;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.ActivityScoped;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.IApiDao;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.ui.base.BasePresenter;

@ActivityScoped
public final class LoaderPresenter extends BasePresenter<LoaderContract.View> implements LoaderContract.Presenter {

    @Inject
    IApiDao mApiDao;

    @Inject
    public LoaderPresenter() {

    }

    @Override
    public void loadData() {
        final LoaderContract.View view = getFragmentView();

        mApiDao.getCurrentIp()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(__ -> view.showLoading())
                .doOnTerminate(() -> view.hideLoading())
                .subscribe(
                        entity -> getFragmentView().setCurrentIp(entity.getCurrentIp()),
                        throwable -> getFragmentView().showError(throwable.getMessage())
                );
    }
}

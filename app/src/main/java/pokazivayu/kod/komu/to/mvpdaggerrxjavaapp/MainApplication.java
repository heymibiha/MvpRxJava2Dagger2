package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.MainGlobalState;


public class MainApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return MainGlobalState.get(this);
    }
}

package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.components;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.MainApplication;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules.ActivityBinderModule;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules.MainApplicationModule;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules.NetModule;

@Singleton
@Component(modules = {

        MainApplicationModule.class,
        ActivityBinderModule.class,
        AndroidSupportInjectionModule.class,
        NetModule.class
})
public interface MainApplicationComponent extends AndroidInjector<MainApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        MainApplicationComponent build();
    }
}

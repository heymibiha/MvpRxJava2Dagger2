package pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.di.modules;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.BuildConfig;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.ApiDaoImpl;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.IApiClient;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.IApiDao;
import pokazivayu.kod.komu.to.mvpdaggerrxjavaapp.net.okhttp.OkHttpApiClientImpl;

@Module
public abstract class NetModule {

    private static final String BASE_URL_NAME = "BaseUrl";

    @Provides
    @Singleton
    @Named(BASE_URL_NAME)
    static String provideBaseUrl() {
        return BuildConfig.SERVER_URL;
    }

    @Provides
    @Singleton
    static IApiClient provideApiClient(@Named(BASE_URL_NAME) String baseUrl, Gson gson) {
        return new OkHttpApiClientImpl(baseUrl, gson);
    }

    @Provides
    @Singleton
    static IApiDao provideApiDao(IApiClient apiClient) {
        return new ApiDaoImpl(apiClient);
    }

    @Provides
    @Singleton
    static Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }
}
